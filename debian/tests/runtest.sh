#!/bin/sh

set -e

# run gophish
gophish

# test if the service started
if ! systemctl is-active -q gophish; then
    echo "FAILURE: gophish service is not active"
    exit 1
fi

# test Web UI response
if ! curl -k -s https://127.0.0.1:3333/login | grep "Please sign in"; then
    echo "FAILURE: Web UI test fails"
    exit 1
fi

# stop the service
gophish-stop

# test if the service stopped correctly
if systemctl is-active -q gophish; then
    echo "FAILURE: gophish is still active"
    exit 1
fi
