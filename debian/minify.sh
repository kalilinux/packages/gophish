#!/bin/sh

set -e

for f in $(ls static/js/src/app/); do
    uglifyjs static/js/src/app/$f -c -o static/js/dist/app/${f%.js}.min.js
done

